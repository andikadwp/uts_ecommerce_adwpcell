import 'package:flutter/material.dart';

import 'package:flutter/cupertino.dart';

class EntryForm extends StatefulWidget {
  @override
  EntryFormState createState() => EntryFormState();
}

//class controller
class EntryFormState extends State<EntryForm> {
  // EntryFormState(this.contact);

  TextEditingController nameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController deskripsiController = TextEditingController();
  TextEditingController fotoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //kondisi

    //rubah
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: new Center(
                  child: new Text("ADWP CELL", textAlign: TextAlign.center)),
              elevation: 0.0,
              backgroundColor: Colors.cyan,
              actions: <Widget>[
                Stack(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: InkResponse(
                        onTap: () {
                          // (user.user != null)
                          //     ? Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => KeranjangPage()))
                          //     : Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => LoginPage()));
                        },
                        child: Tab(
                          icon: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.shopping_cart),
                              Container(
                                alignment: Alignment.center,
                                width: 18,
                                height: 18,
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                child: Text(
                                  "0",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
            body: Padding(
              padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
              child: ListView(
                children: <Widget>[
                  // username
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: usernameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Nama Produk',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  // nama
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: nameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Harga Produk',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // deskripsi
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: deskripsiController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Description',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // foto
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: fotoController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Photo Url',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  new InkWell(
                    onTap: () {
                      // Navigator.push(
                      //     context, MaterialPageRoute(builder: (context) => LauncherPage()));
                    },
                    child: Container(
                      width: 200,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.shade200,
                                offset: Offset(2, 4),
                                blurRadius: 5,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [Colors.blue, Colors.lightBlue[100]])),
                      child: Text(
                        'Tambah Produk',
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}
