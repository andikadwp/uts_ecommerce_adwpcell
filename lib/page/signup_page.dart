import 'package:flutter/material.dart';
import 'package:uts_ecommerce_adwpcell/page/login_page.dart';
// import 'package:megagalery/bloc/user_bloc.dart';
// import 'package:megagalery/model/buat_user_model.dart';
// import 'package:megagalery/validasi/email.dart';
// import 'package:provider/provider.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  // var user = new BuatUserModel();

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
          backgroundColor: Colors.lightBlue[50],
          radius: 70,
          child: Column(
            children: [
              Icon(Icons.android_rounded, size: 85, color: Colors.cyan[400]),
              Text(
                "ADWP CELL",
                style: TextStyle(color: Colors.cyan[400]),
              )
            ],
          )),
    );

    final nama = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Nama',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final tanggalLahir = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Tanggal Lahir',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.of(context)
              .pushReplacement(new MaterialPageRoute(builder: (_) {
            return new LoginPage();
          }));
        },
        padding: EdgeInsets.all(12),
        color: Colors.cyan[400],
        child: Text('Register', style: TextStyle(color: Colors.white)),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Have an acoount? sign in',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        Navigator.of(context)
            .pushReplacement(new MaterialPageRoute(builder: (_) {
          return new LoginPage();
        }));
      },
    );

    final labelPage = FlatButton(
      child: Text(
        'Registrasi',
        style: TextStyle(
            color: Colors.black54, fontSize: 20, fontWeight: FontWeight.bold),
      ),
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 8.0),
            labelPage,
            SizedBox(height: 48.0),
            nama,
            SizedBox(height: 8.0),
            tanggalLahir,
            SizedBox(height: 8.0),
            email,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 24.0),
            loginButton,
            forgotLabel
          ],
        ),
      ),
    );
  }

  // create(BuildContext context) async{
  //   var bloc = Provider.of<UserBloc>(context);
  //   var res = await bloc.create(user);
  //   if (res == null) {
  //     final snackbar = SnackBar(
  //       content: Text("Tidak dapat mendaftarkan pengguna"),
  //     );
  //     _scaffoldKey.currentState.showSnackBar(snackbar);
  //   } else {
  //     Navigator.pop(context);
  //     return;
  //   }
  // }
}
