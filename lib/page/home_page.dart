import 'package:flutter/material.dart';
import 'package:uts_ecommerce_adwpcell/page/Tambah_page.dart';
import 'package:uts_ecommerce_adwpcell/page/contact_page.dart';
import 'package:uts_ecommerce_adwpcell/page/keranjang_page.dart';
import 'package:uts_ecommerce_adwpcell/page/login_page.dart';
import 'package:uts_ecommerce_adwpcell/page/profile_page.dart';
// import 'package:megagalery/bloc/home_bloc.dart';
// import 'package:uts_ecommerce_adwpcell/bloc/keranjang_bloc.dart';
// import 'package:megagalery/bloc/user_bloc.dart';
// import 'package:megagalery/page/contact_page.dart';
// import 'package:megagalery/page/keranjang_page.dart';
// import 'package:megagalery/page/location_page.dart';
// import 'package:megagalery/page/login_page.dart';
// import 'package:megagalery/page/orderlist_page.dart';
// import 'package:megagalery/widgets/kategori_list_widget.dart';
// import 'package:megagalery/widgets/produk_list_widget.dart';
// import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final bloc = Provider.of<HomeBloc>(context);
    // final keranjangBloc = Provider.of<KeranjangBloc>(context);
    // final user = Provider.of<UserBloc>(context);
    return Scaffold(
        backgroundColor: Colors.lightBlue[50],
        appBar: AppBar(
          title: new Center(
              child: new Text("ADWP CELL", textAlign: TextAlign.center)),
          elevation: 0.0,
          backgroundColor: Colors.cyan,
          actions: <Widget>[
            Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: InkResponse(
                    onTap: () {
                      // (user.user != null)
                      //     ? Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => KeranjangPage()))
                      //     : Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => LoginPage()));
                    },
                    child: Tab(
                      icon: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.shopping_cart),
                          Container(
                            alignment: Alignment.center,
                            width: 18,
                            height: 18,
                            decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5))),
                            child: Text(
                              "0",
                              style: TextStyle(color: Colors.white),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
        body: Stack(children: <Widget>[
          Container(
            decoration: new BoxDecoration(
                gradient: LinearGradient(colors: [Colors.cyan, Colors.blue]),
                borderRadius: new BorderRadius.only(
                    bottomLeft: const Radius.circular(40.0),
                    bottomRight: const Radius.circular(40.0))),
            height: 65.0,
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Kategori",
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1,
                      ),
                    ),
                    new InkWell(
                      onTap: () {
                        // bloc.selectedCategory = "todos";
                        // bloc.getProducts();
                      },
                      child: new Padding(
                        padding: new EdgeInsets.all(10.0),
                        child: new Text("Lihat Semua"),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                // KategoriList(
                //   kategori: bloc.kategori,
                // ),
                SizedBox(
                  height: 50,
                ),

                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   //ROW 1
                //   children: [],
                // ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   //ROW 1
                //   children: [
                //     Container(
                //         color: Colors.white,
                //         margin: EdgeInsets.all(25.0),
                //         child: Column(
                //           children: [
                //             Image.network(
                //               "https://foto.kontan.co.id/mF4zyYKUXV8tCn9_odtzbDv8K7U=/smart/2020/06/04/1524062202p.jpg",
                //               cacheHeight: 100,
                //               cacheWidth: 100,
                //             ),
                //             Text("Samsung Galaxy A51"),
                //             Text(
                //               "Rp. 4.500.000",
                //               style: TextStyle(fontWeight: FontWeight.bold),
                //             )
                //           ],
                //         )),
                //     Container(
                //         color: Colors.white,
                //         margin: EdgeInsets.all(25.0),
                //         child: Column(
                //           children: [
                //             Image.network(
                //               "https://foto.kontan.co.id/mF4zyYKUXV8tCn9_odtzbDv8K7U=/smart/2020/06/04/1524062202p.jpg",
                //               cacheHeight: 100,
                //               cacheWidth: 100,
                //             ),
                //             Text("Samsung Galaxy A51"),
                //             Text(
                //               "Rp. 4.500.000",
                //               style: TextStyle(fontWeight: FontWeight.bold),
                //             )
                //           ],
                //         )),
                //   ],
                // ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   //ROW 1
                //   children: [
                //     Container(
                //         color: Colors.white,
                //         margin: EdgeInsets.all(25.0),
                //         child: Column(
                //           children: [
                //             Image.network(
                //               "https://foto.kontan.co.id/mF4zyYKUXV8tCn9_odtzbDv8K7U=/smart/2020/06/04/1524062202p.jpg",
                //               cacheHeight: 100,
                //               cacheWidth: 100,
                //             ),
                //             Text("Samsung Galaxy A51"),
                //             Text(
                //               "Rp. 4.500.000",
                //               style: TextStyle(fontWeight: FontWeight.bold),
                //             )
                //           ],
                //         )),
                //     Container(
                //         color: Colors.white,
                //         margin: EdgeInsets.all(25.0),
                //         child: Column(
                //           children: [
                //             Image.network(
                //               "https://foto.kontan.co.id/mF4zyYKUXV8tCn9_odtzbDv8K7U=/smart/2020/06/04/1524062202p.jpg",
                //               cacheHeight: 100,
                //               cacheWidth: 100,
                //             ),
                //             Text("Samsung Galaxy A51"),
                //             Text(
                //               "Rp. 4.500.000",
                //               style: TextStyle(fontWeight: FontWeight.bold),
                //             )
                //           ],
                //         )),
                //   ],
                // ),
                // Expanded(
                //   child: SingleChildScrollView(
                //     physics: bloc.produk != [] || bloc.produk.length != 0
                //         ? new AlwaysScrollableScrollPhysics()
                //         : new NeverScrollableScrollPhysics(),
                //     scrollDirection: Axis.vertical,
                //     child: ProdukList(produk: bloc.produk),
                //   ),
                // ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 90),
            child: GridView.count(
              crossAxisCount: 2,
              children: [
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://foto.kontan.co.id/mF4zyYKUXV8tCn9_odtzbDv8K7U=/smart/2020/06/04/1524062202p.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Samsung Galaxy A51"),
                        Text(
                          "Rp. 4.500.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://images.tokopedia.net/img/cache/200-square/VqbcmM/2021/3/21/52eeee4c-7fe4-40a3-99a9-29a5f0209fec.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Samsung Galaxy A32"),
                        Text(
                          "Rp. 3.799.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://images.tokopedia.net/img/cache/200-square/VqbcmM/2021/1/8/2dcf3447-186e-428a-815a-260c9aa70a8f.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Samsung Galaxy A02s"),
                        Text(
                          "Rp. 1.900.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://images.tokopedia.net/img/cache/200-square/VqbcmM/2020/12/1/7693437a-f667-483e-b921-be02ec4a2294.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Iphone XR 128 GB"),
                        Text(
                          "Rp. 8.900.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://images.tokopedia.net/img/cache/200-square/VqbcmM/2021/3/7/76ff5584-31cc-44bb-833e-121e3d937598.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Iphone 12 64 GB"),
                        Text(
                          "Rp. 14.290.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://images.tokopedia.net/img/cache/200-square/VqbcmM/2021/1/8/cdd1be5e-d870-44ae-9369-aea2a58c8c60.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Samsung Galaxy A12"),
                        Text(
                          "Rp. 2.700.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://images.tokopedia.net/img/cache/200-square/product-1/2018/9/3/39315521/39315521_83da5aaa-908b-40b1-967d-4426a67590ad_600_600.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Samsung J1 Pro"),
                        Text(
                          "Rp. 2.500.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Image.network(
                          "https://images.tokopedia.net/img/cache/200-square/VqbcmM/2021/3/29/9447eb26-c070-4ef4-9e3e-4c963a125689.jpg",
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text("Samsung Galaxy M02"),
                        Text(
                          "Rp. 1.500.000",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
              ],
            ),
          ),
        ]),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              new Container(
                color: Colors.transparent,
                child: UserAccountsDrawerHeader(
                  accountName: Text("Adwp"),
                  accountEmail: Text("Adwp@gmail.com"),
                ),
              ),
              ListTile(
                title: Text('My Orders'),
                leading: Icon(Icons.shopping_basket),
                onTap: () {
                  // (user.user != null)
                  //     ? Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => OrderListPage()))
                  //     : Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => LoginPage()));
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => KeranjangPage()));
                },
              ),
              ListTile(
                title: Text('Tambah Barang'),
                leading: Icon(Icons.add_alert),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => EntryForm()));
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => ContactPage()));
                },
              ),
              ListTile(
                title: Text('About'),
                leading: Icon(Icons.account_circle_outlined),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ContactPage()));
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => LocationPage()));
                },
              ),
              ListTile(
                title: Text('Profile'),
                leading: Icon(Icons.account_circle),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()));
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => LocationPage()));
                },
              ),
              Divider(),
              ListTile(
                title: Text('Logout'),
                leading: Icon(Icons.exit_to_app),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                  // user.logout();
                  // keranjangBloc.keranjang.clear();
                  // keranjangBloc.total = 0;
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => HomePage()));
                },
              )
              //      ListTile(
              //   title: Text('Login'),
              //   leading: Icon(Icons.exit_to_app),
              //   onTap: () {
              //     Navigator.push(
              //         context,
              //         MaterialPageRoute(
              //             builder: (context) => LoginPage()));
              //   },
              // ),
            ],
          ),
        ));
  }
}
