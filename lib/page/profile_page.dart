import 'package:flutter/material.dart';
import 'package:uts_ecommerce_adwpcell/main.dart';
import 'package:uts_ecommerce_adwpcell/page/home_page.dart';
import 'package:uts_ecommerce_adwpcell/page/get_model.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  MyHomePage myHomePage;
  UserGet userGet;

@override
void initState() { 
  super.initState();
  UserGet.connectToApiUser('1').then((value) {
    setState(() {
      userGet = value;
    });
  });
}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
       child: Scaffold(
         body: Center(
           child: Container(
             height: MediaQuery.of(context).size.height,
             color: Colors.blue[100],
             alignment: Alignment.center,
             padding: EdgeInsets.all(30),
             child: Column(
               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
               children: [
                 Text(
                   "WELCOME",
                   style: TextStyle(fontSize: 48, color: Color(0xff0563BA)), 
                 ),
                 Container(
                   child: Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(70.0),
                      child: Image.network(
                        userGet.avatar,
                        cacheHeight: 128,
                        cacheWidth: 128,
                        fit: BoxFit.cover,
                      ),
                    ),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Text(
                     userGet.firstname + " " + userGet.lastname,
                     style: 
                          TextStyle(fontSize: 24, color: Color(0xff0563BA)),
                   ),
                 ),
                 Text(
                   userGet.email,
                   style: TextStyle(fontSize: 18, color: Colors.black),
                 ) 
                ]   
               )
              ) 
           ],)
          ),
         )
        )
    );
  }
}