import 'package:flutter/material.dart';
import 'package:uts_ecommerce_adwpcell/page/home_page.dart';
import 'package:uts_ecommerce_adwpcell/page/signup_page.dart';
// import 'package:uts_ecommerce_adwpcell/bloc/user_bloc.dart';
// import 'package:uts_ecommerce_adwpcell/model/authenticate_user_model.dart';
// import 'package:uts_ecommerce_adwpcell/page/signup_page.dart';
// import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var email = "";
  var password = "";

  // final GoogleSignIn _googleSignIn = GoogleSignIn();
  // final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: Container(
          // radius: 80,
          child: Column(
        children: [
          Icon(
            Icons.android_rounded,
            size: 120,
            color: Colors.cyan[300],
          )
          // Image.asset("assets/1234.jpeg")
        ],
      )),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.of(context)
              .pushReplacement(new MaterialPageRoute(builder: (_) {
            return new HomePage();
          }));
        },
        padding: EdgeInsets.all(12),
        color: Colors.cyan[200],
        child: Text('Log In', style: TextStyle(color: Colors.black)),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );
    final regisLabel = FlatButton(
      child: Text(
        "Don't have an account? sign up",
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        Navigator.of(context)
            .pushReplacement(new MaterialPageRoute(builder: (_) {
          return new SignupPage();
        }));
      },
    );
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
            child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
              logo,
              SizedBox(height: 48.0),
              email,
              SizedBox(height: 8.0),
              password,
              SizedBox(height: 24.0),
              loginButton,
              forgotLabel,
              regisLabel
            ])));
//        RaisedButton(
//          color: Theme.of(context).primaryColor,
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            crossAxisAlignment: CrossAxisAlignment.center,
//            children: <Widget>[
//              Image.asset("assets/icons/logo_google.png", height: 36,),
//              Text(
//                "Login Google",
//                style: TextStyle(color: Colors.white),
//              )
//            ],
//          ),
//          onPressed: () => loginGoogle(),
//        ),
  }

  // authenticate(BuildContext context) async {
  //   var bloc = Provider.of<UserBloc>(context);

  //   var user = await bloc.authenticate(AuthenticateModel(
  //     email: email,
  //     password: password,
  //   ));
  //   if (user == null) {
  //     final snackbar = SnackBar(
  //       content: Text("Nama pengguna atau kata sandi tidak valid"),
  //     );
  //     _scaffoldKey.currentState.showSnackBar(snackbar);
  //   } else {
  //     Navigator.pop(context);
  //     return;
  //   }
  // }

//  Future<FirebaseUser> _handleSignIn() async {
//    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
//    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
//
//    final AuthCredential credential = GoogleAuthProvider.getCredential(
//      accessToken: googleAuth.accessToken,
//      idToken: googleAuth.idToken,
//    );
//
//    final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
//    print("signed in " + user.displayName);
//    return user;
//  }
//
//  loginGoogle() async{
//    await _handleSignIn()
//        .then((FirebaseUser user) => print(user))
//        .catchError((e) => print(e));
//  }
}
